﻿// Triangle script
// Depends on QuelleStd

try {
    // Create new document
    var document = app.documents.add();
    
    with(document.documentPreferences) {
        pageHeight = "800pt";
        pageWidth = "600pt";
        pageOrientation = PageOrientation.portrait;
        pagesPerDocument = 1;
    }

    // Default Layer reneame to Master;
    document.layers[0].name="Master";

    // Create  new Layer
    try {
        document.layers.item("Shape").name;
    } catch (e) {
        document.layers.add({"name":"Shape"});
    }

    // Create new Color
    var colorLila = document.colors.add({"name":"Lila", "colorValue":[41, 52, 0, 31]});

    // Select Layer and create template frames
    document.activeLayer = document.layers.item("Master");
    createStdFrames( document );
    
    // Set working Layer
    document.activeLayer = document.layers.item("Shape");
    
    var page = document.pages[0];
    var rect;
    
    for (var i=0; i<500; i++)
    {
        rect = page.rectangles.add({geometricBounds:[0, 10, 10+i*10, 20+i*10]});
        rect.move(undefined, [50, 100]);
        rect.rotationAngle -= 55;
    }

    rect = page.rectangles.add({geometricBounds:[0, 10, 20, 20]});
    rect.fillColor = colorLila;

    page.polygons.add({geometricBounds:[60,60,40,20]});
    

    var halfWidth = document.documentPreferences.pageWidth/2
    
    for (i=0; i<1; i++)
    {
        page.polygons.add({geometricBounds:[60,halfWidth,100,halfWidth+100]});
    }


    // Save Document and Close it after save is done.
    document.save(File("C:/IOR/files/POC_"+getName()+".indd"));
    document.close();
    
	"HD Test";    
    
} catch (e) {
    alert ("Something went worng. : " +e);
}



function getName() {
	var date = new Date();
	var day = (date.getDate() > 10 ) ? date.getDate() : ("0" + date.getDate() ),
		month = ((date.getMonth()+1) > 10 ) ? (date.getMonth()+1) : ("0" + (date.getMonth()+1) ),
		year = (date.getFullYear() > 10 ) ? date.getFullYear() : ("0" + date.getFullYear() ),
		hours = (date.getHours() > 10 ) ? date.getHours() : ("0" + date.getHours() ),
		minutes = (date.getMinutes() > 10 ) ? date.getMinutes() : ("0" + date.getMinutes() );
	
	return year + month + day + "_" + hours + minutes;
};

function createStdFrames(document)
{
    if (!document)
    {
        alert("There is no active document.");
        return;
    }

    // ### Create document Styles ###

    // Standard document color
    var color1 = document.colors.item("C=100 M=0 Y=0 K=0");
    var color2 = document.colors.item("C=15 M=100 Y=100 K=0");

    try {
        document.paragraphStyles.add({
            name:"Title",
            pointSize:"18px",
            justification:Justification.centerAlign,
            fillColor: color1
        });
    } catch (e) {
        // Do nothing. Style exist.
    }

    // -- Create Footer Style
   try {
        document.paragraphStyles.add({
            name:"Footer",
            pointSize:"14px",
            justification:Justification.rightAlign,
            fillColor: color2,
        });
    } catch (e) {
        // Do nothing. Style exist.
    }



    // ### Get Document pages ###
    var pages = document.pages;

    if (pages.length >= 1)
    {
        var page = pages.item(0);
        var frame;
        
        frame = page.textFrames.add (
            {
                geometricBounds : ["40px", "40px", "70px", "555px"],
                contents: "JS Auto Document",
        });
    
        // Set paragraph style
        frame.insertionPoints[-1].appliedParagraphStyle  = "Title";

        /*
             y1	x1	y2	x2
            [0]	[1]	[2]	[3]
             y	   x	x+w    y+h   
         */
        frame = page.textFrames.add (
            {
                geometricBounds : [document.documentPreferences.pageHeight-20, document.documentPreferences.pageWidth/2, document.documentPreferences.pageHeight, document.documentPreferences.pageWidth-5],
                contents: "Heidelberger Druckmaschinen AG",
        });
    
        // Set paragraph style
        frame.insertionPoints[-1].appliedParagraphStyle  = "Footer";

    }
}