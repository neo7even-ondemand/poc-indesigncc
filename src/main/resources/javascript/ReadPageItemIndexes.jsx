﻿var clientDebug = false;
var scriptArgs = app.scriptArgs;

if (scriptArgs.isDefined("docName")) {
	var docName = scriptArgs.get("docName");
	var doc = app.documents.itemByName(docName);
} else {
	var isClientExecution = true;
	var doc = app.activeDocument;
}

try {

	//json2.js
	//2016-05-01
	//Public Domain.
	//NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
	//See http://www.JSON.org/js.html
	//This code should be minified before deployment.
	//See http://javascript.crockford.com/jsmin.html
	
	//USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
	//NOT CONTROL.
	
	if (typeof JSON !== "object") {
		JSON = {};
	}
	
	(function() {
		"use strict";
	
		var rx_one = /^[\],:{}\s]*$/;
		var rx_two = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g;
		var rx_three = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g;
		var rx_four = /(?:^|:|,)(?:\s*\[)+/g;
		var rx_escapable = /[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
		var rx_dangerous = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
	
		function f(n) {
			// Format integers to have at least two digits.
			return n < 10 ? "0" + n : n;
		}
	
		function this_value() {
			return this.valueOf();
		}
	
		if (typeof Date.prototype.toJSON !== "function") {
	
			Date.prototype.toJSON = function() {
	
				return isFinite(this.valueOf()) ? this.getUTCFullYear() + "-"
						+ f(this.getUTCMonth() + 1) + "-" + f(this.getUTCDate())
						+ "T" + f(this.getUTCHours()) + ":"
						+ f(this.getUTCMinutes()) + ":" + f(this.getUTCSeconds())
						+ "Z" : null;
			};
	
			Boolean.prototype.toJSON = this_value;
			Number.prototype.toJSON = this_value;
			String.prototype.toJSON = this_value;
		}
	
		var gap;
		var indent;
		var meta;
		var rep;
	
		function quote(string) {
	
			rx_escapable.lastIndex = 0;
			return rx_escapable.test(string) ? "\""
					+ string.replace(rx_escapable,
							function(a) {
								var c = meta[a];
								return typeof c === "string" ? c : "\\u"
										+ ("0000" + a.charCodeAt(0).toString(16))
												.slice(-4);
							}) + "\"" : "\"" + string + "\"";
		}
	
		function str(key, holder) {
	
			var i; // The loop counter.
			var k; // The member key.
			var v; // The member value.
			var length;
			var mind = gap;
			var partial;
			var value = holder[key];
	
			if (value && typeof value === "object"
					&& typeof value.toJSON === "function") {
				value = value.toJSON(key);
			}
	
			if (typeof rep === "function") {
				value = rep.call(holder, key, value);
			}
	
			switch (typeof value) {
			case "string":
				return quote(value);
	
			case "number":
	
				return isFinite(value) ? String(value) : "null";
	
			case "boolean":
			case "null":
	
				return String(value);
	
			case "object":
	
				if (!value) {
					return "null";
				}
	
				gap += indent;
				partial = [];
	
				if (Object.prototype.toString.apply(value) === "[object Array]") {
	
					length = value.length;
					for (i = 0; i < length; i += 1) {
						partial[i] = str(i, value) || "null";
					}
	
					v = partial.length === 0 ? "[]" : gap ? "[\n" + gap
							+ partial.join(",\n" + gap) + "\n" + mind + "]" : "["
							+ partial.join(",") + "]";
					gap = mind;
					return v;
				}
	
				if (rep && typeof rep === "object") {
					length = rep.length;
					for (i = 0; i < length; i += 1) {
						if (typeof rep[i] === "string") {
							k = rep[i];
							v = str(k, value);
							if (v) {
								partial.push(quote(k) + (gap ? ": " : ":") + v);
							}
						}
					}
				} else {
	
					for (k in value) {
						if (Object.prototype.hasOwnProperty.call(value, k)) {
							v = str(k, value);
							if (v) {
								partial.push(quote(k) + (gap ? ": " : ":") + v);
							}
						}
					}
				}
	
				v = partial.length === 0 ? "{}" : gap ? "{\n" + gap
						+ partial.join(",\n" + gap) + "\n" + mind + "}" : "{"
						+ partial.join(",") + "}";
				gap = mind;
				return v;
			}
		}
	
		// If the JSON object does not yet have a stringify method, give it one.
	
		if (typeof JSON.stringify !== "function") {
			meta = { // table of character substitutions
				"\b" : "\\b",
				"\t" : "\\t",
				"\n" : "\\n",
				"\f" : "\\f",
				"\r" : "\\r",
				"\"" : "\\\"",
				"\\" : "\\\\"
			};
			JSON.stringify = function(value, replacer, space) {
	
				var i;
				gap = "";
				indent = "";
	
				if (typeof space === "number") {
					for (i = 0; i < space; i += 1) {
						indent += " ";
					}
	
				} else if (typeof space === "string") {
					indent = space;
				}
	
				rep = replacer;
				if (replacer
						&& typeof replacer !== "function"
						&& (typeof replacer !== "object" || typeof replacer.length !== "number")) {
					throw new Error("JSON.stringify");
				}
	
				// Make a fake root object containing our value under the key of "".
				// Return the result of stringifying the value.
	
				return str("", {
					"" : value
				});
			};
		}
	
		// If the JSON object does not yet have a parse method, give it one.
	
		if (typeof JSON.parse !== "function") {
			JSON.parse = function(text, reviver) {
	
				// The parse method takes a text and an optional reviver function,
				// and returns
				// a JavaScript value if the text is a valid JSON text.
	
				var j;
	
				function walk(holder, key) {
	
					// The walk method is used to recursively walk the resulting
					// structure so
					// that modifications can be made.
	
					var k;
					var v;
					var value = holder[key];
					if (value && typeof value === "object") {
						for (k in value) {
							if (Object.prototype.hasOwnProperty.call(value, k)) {
								v = walk(value, k);
								if (v !== undefined) {
									value[k] = v;
								} else {
									delete value[k];
								}
							}
						}
					}
					return reviver.call(holder, key, value);
				}
	
				// Parsing happens in four stages. In the first stage, we replace
				// certain
				// Unicode characters with escape sequences. JavaScript handles many
				// characters
				// incorrectly, either silently deleting them, or treating them as
				// line endings.
	
				text = String(text);
				rx_dangerous.lastIndex = 0;
				if (rx_dangerous.test(text)) {
					text = text.replace(rx_dangerous,
							function(a) {
								return "\\u"
										+ ("0000" + a.charCodeAt(0).toString(16))
												.slice(-4);
							});
				}
	
				if (rx_one.test(text.replace(rx_two, "@").replace(rx_three, "]")
						.replace(rx_four, ""))) {
	
					j = eval("(" + text + ")");
	
					return (typeof reviver === "function") ? walk({
						"" : j
					}, "") : j;
				}
	
				throw new SyntaxError("JSON.parse");
			};
		}
	}());
	// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	log("Script ReadPageItemIndexes start");

	var currentIndex = 0;

	var sortItemsByIndex = function(a, b) {
		 var x = a.index; 
		 var y = b.index;
		 return x - y;
	};

	var addItemToCollection = function(collection, item, index) {
		var pageItemIndex = new Object();
		pageItemIndex.id = item.id;
		pageItemIndex.item = item;
		pageItemIndex.index = index;
	
		collection.push(pageItemIndex);
		return pageItemIndex;
	}
		
	var collectContainerIndicies = function(itemsCollection) {
		var items = itemsCollection.everyItem().getElements();
		var indexes = itemsCollection.everyItem().index;
		
		var collection = new Array();
		for (var i = 0; i < items.length; i++) {
			addItemToCollection(collection, items[i], indexes[i]);
		}

		collection.sort(sortItemsByIndex);
				
		var subCollections = new Array();
		for (var i = 0; i < collection.length; i++) {
			collection[i].index = currentIndex++;
			if (collection[i].item instanceof Group) {
				subCollections = subCollections.concat(collectContainerIndicies(collection[i].item.pageItems));
			} 
		}
		collection = collection.concat(subCollections);
		return collection;
	}

    log("Test 3");

    if (doc.pageItems)
    {
        log("Page Item CNT ");
    }
    else
    {
    	log("Page items does not exist.");
    }
    

	var pageItemIndexes= collectContainerIndicies(doc.pageItems);

	for (var i = 0; i < pageItemIndexes.length; i++) {
		//pageItemIndexes[i].item.label =  pageItemIndexes[i].item.id + " - "+pageItemIndexes[i].index;
		delete pageItemIndexes[i].item;
	}

	log("Build JSON Output");
	var resultObject = new Object();
	resultObject.pageItemIndexes = pageItemIndexes;
	var json = JSON.stringify(resultObject);
	log("Finished Script");
	result = json;
} catch (e) {
	if (isClientExecution) {
		alert(e + " @ " + e.line);
	} else {
		app.consoleerr(e + " @ " + e.line);
        result = "Error: " + e + " @ " + e.line;
	}
}



function log(message) {
    if (isClientExecution) {
    	if (clientDebug) {
        	alert(message);
        }
    } else {
        app.consoleout(message);
    }
}