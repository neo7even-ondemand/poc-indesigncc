package com.heidelberg.neoseven.poc.indesigncc.soap;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adobe.ns.indesign.soap.Data;
import com.adobe.ns.indesign.soap.IDSPScriptArg;
import com.adobe.ns.indesign.soap.RunScriptParameters;
import com.heidelberg.neoseven.poc.indesigncc.PocIndesignccApplication;
import com.heidelberg.neoseven.service.wsdl.Service;
import com.heidelberg.neoseven.service.wsdl.ServicePortType;


@RestController
@RequestMapping("/soap")
public class SoapController {
	
	private static final QName SERVICE_NAME = new QName("http://localhost:12345/Service.wsdl", "Service");
	
	@GetMapping("")
	public Map<String, String> execute() {
		long startTime = System.currentTimeMillis();
		URL wsdlURL = Service.WSDL_LOCATION;

		Service ss = new Service(wsdlURL, SERVICE_NAME);
		ServicePortType port = ss.getService();

		System.out.println("Invoking beginSession...");
		long _beginSession__return = port.beginSession();
		System.out.println("beginSession.result=" + _beginSession__return);
		
		
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource( PocIndesignccApplication.TEST_FILE ).getFile());
		String testFile = file.getAbsolutePath();		
		
        if (testFile == null) {
            java.util.logging.Logger.getLogger(SoapController.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the test file from {0}", "classpath:javascript/Triangle.jsx");
        }
		
		System.out.println("Invoking runScript...");
		
		//IDSPScriptArg scriptARgs = new IDSPScriptArg();
		//scriptARgs.setName("docName");
		//scriptARgs.setValue("C:/IOR/files/SCRIPT-TEST.indd");
		
		RunScriptParameters _runScript_runScriptParameters = new RunScriptParameters();
		//_runScript_runScriptParameters.getScriptArgs().add(scriptARgs);
		//_runScript_runScriptParameters.setScriptText(new JAXBElement<String>(new QName("scriptText"), String.class, "app.consoleout('Execute Test');'Triangle.jsx'"));
		_runScript_runScriptParameters.setScriptLanguage( new JAXBElement<String>(new QName("scriptLanguage"), String.class, "javascript") );
		_runScript_runScriptParameters.setScriptFile( new JAXBElement<String>(new QName("scriptFile"), String.class, testFile) );
		
		Holder<Integer> _runScript_errorNumber = new Holder<Integer>();
		Holder<String> _runScript_errorString = new Holder<String>();
		Holder<Data> _runScript_scriptResult = new Holder<Data>();
		port.runScript(_runScript_runScriptParameters, _runScript_errorNumber, _runScript_errorString,
				_runScript_scriptResult);

		System.out.println("runScript._runScript_errorNumber=" + _runScript_errorNumber.value);
		System.out.println("runScript._runScript_errorString=" + _runScript_errorString.value);
		System.out.println("runScript._runScript_scriptResult=" + _runScript_scriptResult.value);

	
		System.out.println("Invoking endSession...");
		int _endSession__return = port.endSession(_beginSession__return);
		System.out.println("endSession.result=" + _endSession__return);

		Map<String, String> result = new HashMap<>();
		result.put("result", (String)_runScript_scriptResult.value.getData());
		result.put("executionTime", Long.toString(System.currentTimeMillis() - startTime) + " ms");
			
		return result;
	}
}
