package com.heidelberg.neoseven.poc.indesigncc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocIndesignccApplication {
	
	public static final String TEST_FILE = "javascript/Triangle.jsx";
//	public static final String TEST_FILE = "javascript/ReadPageItemIndexes.jsx";

	public static void main(String[] args) {
		SpringApplication.run(PocIndesignccApplication.class, args);
	}
}
