package com.heidelberg.neoseven.poc.indesigncc.corba;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.omg.CORBA.ORB;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.adobe.ids.IdsException;
import com.adobe.ids.OptArg;
import com.adobe.ids.VariableType;
import com.adobe.ids.VariableTypeUtils;
import com.adobe.ids.basics.Application;
import com.adobe.ids.basics.ApplicationHelper;
import com.adobe.ids.basics.ScriptArg;
import com.adobe.ids.enums.kScriptLanguageJavascript;
import com.heidelberg.neoseven.poc.indesigncc.PocIndesignccApplication;
import com.heidelberg.neoseven.poc.indesigncc.soap.SoapController;

@RestController
@RequestMapping("/corba")
public class CorbaController {

	@RequestMapping(value = "", method = RequestMethod.GET)
	public Map<String,String> execute() throws IdsException, IOException {
		
		long startTime = System.currentTimeMillis();
		try {
			System.out.println("Service started.");
			
			String iorString = FileUtils.readFileToString(new File("C:/IOR/iorTest.txt"), "UTF-8");
			System.out.println("IOR Loaded.");
			
			System.out.println(iorString);
			
			ORB orb = ORB.init(new String[] {}, null);
			System.out.println("CORBA Initialized.");
			
			org.omg.CORBA.Object appObj = orb.string_to_object(iorString);
			System.out.println("Corba iorString converted to Object.");
			
			Application app = ApplicationHelper.narrow(appObj);
			//app.consoleout("Connected"); 
			
			
//        VariableType vtScript = VariableTypeUtils.createString("app.consoleout(\"Printed from Script\");\n"
//                      + "if (scriptArgs.isDefined(\"contentOfScriptArg\")) {\n" 
//                      + "    var someArg = scriptArgs.get(\"contentOfScriptArg\");\n"
//                      + " app.consoleout(someArg);\n"
//                      + "}");
			
			
			
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource(PocIndesignccApplication.TEST_FILE).getFile());
			String testFile = file.getAbsolutePath();
			//String testFile = FileUtils.readFileToString(file, "UTF-8");		
			
			if (testFile == null) {
			    java.util.logging.Logger.getLogger(SoapController.class.getName())
			        .log(java.util.logging.Level.INFO, 
			             "Can not initialize the test file from {0}", PocIndesignccApplication.TEST_FILE);
			}
			
			
			
			String strcmd = FileUtils.readFileToString(file, "UTF-8");
			
			//VariableType vtScript = VariableTypeUtils.createFile(testFile);
			VariableType vtScript = VariableTypeUtils.createString(strcmd);

			//ScriptArg scriptArgs = app.getScriptArgs();
            //scriptArgs.set("docName", String.valueOf("C:\\IOR\\files\\POC_20180802_01052.indd"));

			VariableType resultVT = app.doScript(
					vtScript
					, OptArg.makeScriptLanguageEnum(kScriptLanguageJavascript.value) 
					, OptArg.noVariableTypeSeq()
					, OptArg.noUndoModesEnum()
					, OptArg.noString()
			);
			
			
			
			String jsonData = resultVT.asString();
			
			//ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
			//SomeJavaClass result = objectMapper.readValue(jsonData, SomeJavaClass.class);
			
			
			orb.shutdown(true);
			orb.destroy();
			
			
			Map<String, String> result = new HashMap<>();
			result.put("result", jsonData);
			result.put("executionTime.", Long.toString(System.currentTimeMillis() - startTime) + " ms");
			
			return result;
		} catch (IdsException e) {
			System.out.println(e.errorCode + ": " + e.errorMsg);
			e.printStackTrace();
			
			Map<String, String> result = new HashMap<>();
			result.put("exception.", e.errorMsg);
			result.put("executionTime.", Long.toString(System.currentTimeMillis() - startTime) + " ms");
			
			return result;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			Map<String, String> result = new HashMap<>();
			result.put("exception.", e.getMessage());
			result.put("executionTime.", Long.toString(System.currentTimeMillis() - startTime) + " ms");
			
			return result;
		}
		
	}
}
