
package com.adobe.ns.indesign.soap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sessionID" type="{http://ns.adobe.com/InDesign/soap/}SessionID"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sessionID"
})
@XmlRootElement(name = "BeginSessionResponse")
public class BeginSessionResponse {

    @XmlSchemaType(name = "unsignedInt")
    protected long sessionID;

    /**
     * Ruft den Wert der sessionID-Eigenschaft ab.
     * 
     */
    public long getSessionID() {
        return sessionID;
    }

    /**
     * Legt den Wert der sessionID-Eigenschaft fest.
     * 
     */
    public void setSessionID(long value) {
        this.sessionID = value;
    }

}
