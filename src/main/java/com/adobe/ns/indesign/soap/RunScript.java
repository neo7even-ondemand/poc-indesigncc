
package com.adobe.ns.indesign.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="runScriptParameters" type="{http://ns.adobe.com/InDesign/soap/}RunScriptParameters" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "runScriptParameters"
})
@XmlRootElement(name = "RunScript")
public class RunScript {

    @XmlElementRef(name = "runScriptParameters", type = JAXBElement.class, required = false)
    protected JAXBElement<RunScriptParameters> runScriptParameters;

    /**
     * Ruft den Wert der runScriptParameters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RunScriptParameters }{@code >}
     *     
     */
    public JAXBElement<RunScriptParameters> getRunScriptParameters() {
        return runScriptParameters;
    }

    /**
     * Legt den Wert der runScriptParameters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RunScriptParameters }{@code >}
     *     
     */
    public void setRunScriptParameters(JAXBElement<RunScriptParameters> value) {
        this.runScriptParameters = value;
    }

}
