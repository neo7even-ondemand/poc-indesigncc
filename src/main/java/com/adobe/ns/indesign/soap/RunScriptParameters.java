
package com.adobe.ns.indesign.soap;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r RunScriptParameters complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RunScriptParameters"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="scriptText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="scriptLanguage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="scriptFile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="scriptArgs" type="{http://ns.adobe.com/InDesign/soap/}IDSP-ScriptArg" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RunScriptParameters", propOrder = {
    "scriptText",
    "scriptLanguage",
    "scriptFile",
    "scriptArgs"
})
public class RunScriptParameters {

    @XmlElementRef(name = "scriptText", type = JAXBElement.class, required = false)
    protected JAXBElement<String> scriptText;
    @XmlElementRef(name = "scriptLanguage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> scriptLanguage;
    @XmlElementRef(name = "scriptFile", type = JAXBElement.class, required = false)
    protected JAXBElement<String> scriptFile;
    protected List<IDSPScriptArg> scriptArgs;

    /**
     * Ruft den Wert der scriptText-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getScriptText() {
        return scriptText;
    }

    /**
     * Legt den Wert der scriptText-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setScriptText(JAXBElement<String> value) {
        this.scriptText = value;
    }

    /**
     * Ruft den Wert der scriptLanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getScriptLanguage() {
        return scriptLanguage;
    }

    /**
     * Legt den Wert der scriptLanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setScriptLanguage(JAXBElement<String> value) {
        this.scriptLanguage = value;
    }

    /**
     * Ruft den Wert der scriptFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getScriptFile() {
        return scriptFile;
    }

    /**
     * Legt den Wert der scriptFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setScriptFile(JAXBElement<String> value) {
        this.scriptFile = value;
    }

    /**
     * Gets the value of the scriptArgs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scriptArgs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScriptArgs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IDSPScriptArg }
     * 
     * 
     */
    public List<IDSPScriptArg> getScriptArgs() {
        if (scriptArgs == null) {
            scriptArgs = new ArrayList<IDSPScriptArg>();
        }
        return this.scriptArgs;
    }

}
