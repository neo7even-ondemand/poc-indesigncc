
package com.adobe.ns.indesign.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="errorNumber" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="errorString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="scriptResult" type="{http://ns.adobe.com/InDesign/soap/}Data" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "errorNumber",
    "errorString",
    "scriptResult"
})
@XmlRootElement(name = "RunScriptResponse")
public class RunScriptResponse {

    protected int errorNumber;
    @XmlElementRef(name = "errorString", type = JAXBElement.class, required = false)
    protected JAXBElement<String> errorString;
    @XmlElementRef(name = "scriptResult", type = JAXBElement.class, required = false)
    protected JAXBElement<Data> scriptResult;

    /**
     * Ruft den Wert der errorNumber-Eigenschaft ab.
     * 
     */
    public int getErrorNumber() {
        return errorNumber;
    }

    /**
     * Legt den Wert der errorNumber-Eigenschaft fest.
     * 
     */
    public void setErrorNumber(int value) {
        this.errorNumber = value;
    }

    /**
     * Ruft den Wert der errorString-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getErrorString() {
        return errorString;
    }

    /**
     * Legt den Wert der errorString-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setErrorString(JAXBElement<String> value) {
        this.errorString = value;
    }

    /**
     * Ruft den Wert der scriptResult-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Data }{@code >}
     *     
     */
    public JAXBElement<Data> getScriptResult() {
        return scriptResult;
    }

    /**
     * Legt den Wert der scriptResult-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Data }{@code >}
     *     
     */
    public void setScriptResult(JAXBElement<Data> value) {
        this.scriptResult = value;
    }

}
